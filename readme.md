Dynamics CRM PHP Connector Redis Cache
=======

Add Redis cache functionality to Dynamics CRM PHP Connector

Instalation
------------
Add the library to your project using composer.

Add dependency to your composer.json:

    "sixdg/dynamics-crm-php-connector-redis-cache": "0.1.0"

Basic Setup
------------

use Sixdg\RedisCache\Client;

//by default it connects to local Redis server
$cacheClient = new Client();

//pass the client to the CRM Connector
$connector = new \Sixdg\DynamicsCRMConnector\DynamicsCRMConnector(
    $config,
    $aspectKernel,
    $logger,
    $cacheClient
);

Now the CRM connector will use it to cache meta data information.
