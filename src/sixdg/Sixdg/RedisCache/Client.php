<?php

namespace Sixdg\RedisCache;

use Sixdg\DynamicsCRMConnector\Interfaces\CacheInterface;

/**
 * Stores information in Redis using Predis
 *
 * @author leandro.miranda
 */
Class Client extends \Predis\Client implements CacheInterface
{

    /**
     * Defines a prefix to be used by the library to avoid key conflicts
     *
     * @return string
     */
    public function getKeyPrefix()
    {
        return 'D6G';
    }

    /**
     *
     * @param  string $key
     * @return mixed
     */
    public function get($key)
    {
        return parent::get($key);
    }

    /**
     *
     * @param  string $key
     * @return bool
     */
    public function exists($key)
    {
        return parent::exists($key);
    }

    /**
     *
     * @param  string $key
     * @param  mixed  $value
     * @param  int    $expiration Expiration time in seconds
     * @return bool
     */
    public function set($key, $value, $expiration = 0)
    {
        $return = parent::set($key, $value);

        return $return and $this->expire($key, $expiration);
    }

}
